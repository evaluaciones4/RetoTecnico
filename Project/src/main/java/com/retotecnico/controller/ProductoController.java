package com.retotecnico.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.retotecnico.model.Producto;
import com.retotecnico.model.ResultInsertProducto;
import com.retotecnico.service.ProductoService;
import com.retotecnico.util.Constantes;
import com.retotecnico.util.Util;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path=Constantes.PATH_APINAME)
public class ProductoController {
		
	@Autowired
	private ProductoService productoService;
	
	@ApiOperation(value = "Método que Inserta un Producto, y como respuesta, muestra una lista de productos registrados en el día (según la fecha de registro)")
	@PostMapping()
	public ResponseEntity<ResultInsertProducto> insertProducto(@RequestBody Producto producto) {		
		try {
			if(producto ==null)
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "Debe enviar los datos del Producto."));
			else if(producto.getIdProducto() == null || producto.getIdProducto()<=0 || !Util.isNumeric(producto.getIdProducto().toString()))
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "Valor del identificador del producto (idProducto), no válido"));
			else if(producto.getNombre()==null || producto.getNombre().trim().isEmpty())
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "Debe enviar el nombre (nombre) del Producto."));
			else if(producto.getFechaRegistro()==null || producto.getFechaRegistro().trim().isEmpty())				
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "Debe enviar la fecha de registro (fechaRegistro) del Producto."));
			else if(!Util.isValidFormatoFecha(producto.getFechaRegistro(), "yyyyMMdd"))
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "El formato de la fehca de registro (fechaRegistro), no es válida. El formato esperado es YYYYMMDD"));
						
			ResultInsertProducto resultInsertProducto = productoService.insertProducto(producto);			
			return ResponseEntity.ok(resultInsertProducto);
						
		} catch (Exception e) {			
			e.printStackTrace();
			log.error(e.getMessage(), e);	
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResultInsertProducto(1, "Error interno"));
		}
	}	
	
	
}
