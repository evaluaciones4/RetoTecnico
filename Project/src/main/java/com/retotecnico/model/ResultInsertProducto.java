package com.retotecnico.model;

import java.util.List;

public class ResultInsertProducto {

	private Integer codigoRespuesta;
	private String mensajeRespuesta;
	private List<Producto> listProductos;
	
	public ResultInsertProducto() {
		super();
	}
	
	public ResultInsertProducto(Integer codigoRespuesta, String mensajeRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	public ResultInsertProducto(Integer codigoRespuesta, String mensajeRespuesta, List<Producto> listProductos) {
		this.codigoRespuesta = codigoRespuesta;
		this.mensajeRespuesta = mensajeRespuesta;
		this.listProductos = listProductos;
	}
	
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public List<Producto> getListProductos() {
		return listProductos;
	}
	public void setListProductos(List<Producto> listProductos) {
		this.listProductos = listProductos;
	}
}
