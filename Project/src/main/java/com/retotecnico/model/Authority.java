package com.retotecnico.model;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.retotecnico.util.AuthorityName;

public class Authority {
	
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	private AuthorityName name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AuthorityName getName() {
		return name;
	}

	public void setName(AuthorityName name) {
		this.name = name;
	};
}
