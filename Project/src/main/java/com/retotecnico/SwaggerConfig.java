package com.retotecnico;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
	
@Configuration
public class SwaggerConfig {
	@Bean
    Docket api() {
        return new Docket(DocumentationType.OAS_30)        		
                .select()                
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("(?!/error.*).*"))
                .build()
                .apiInfo(apiInfo())           
                ;
    }
	
	ApiInfo apiInfo() {
        return new ApiInfo(
				"API-RETO TÉCNICO",
				""
			  + "",
				"1.0",
				"",
				new Contact("", "", ""),
				"",
				"",
				Collections.emptyList()
				);
    }
    
}
