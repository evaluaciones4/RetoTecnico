package com.retotecnico.security;

import org.springframework.security.core.GrantedAuthority;

import com.retotecnico.model.Authority;

public class SecurityAuthority implements GrantedAuthority{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Authority authority;
	
	public SecurityAuthority(Authority authority) {
		this.authority = authority;
	}

	@Override
	public String getAuthority() {
		return authority.getName().toString();
	}

}
