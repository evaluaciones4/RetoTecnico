package com.retotecnico.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.retotecnico.model.Usuario;
import com.retotecnico.repository.custom.UsuarioRepositoryCustom;
import com.retotecnico.security.SecurityUser;

@org.springframework.stereotype.Component("securityUserDetailsService")
public class SecurityUserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UsuarioRepositoryCustom usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> optUsuario = Optional.ofNullable(null);
		try {
			optUsuario = usuarioRepository.buscarByUserName(username);
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		if(optUsuario.isPresent())
			return new SecurityUser(optUsuario.get());
		
		throw new UsernameNotFoundException("User not fount: "+ username);
	}
	
}
