package com.retotecnico.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retotecnico.model.Producto;
import com.retotecnico.model.ResultInsertProducto;
import com.retotecnico.repository.custom.ProductoRepositoryCustom;
import com.retotecnico.service.ProductoService;

@Service("productoService")
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoRepositoryCustom productoRepositoryCustom;
	
	@Override
	public ResultInsertProducto insertProducto(Producto producto) throws Exception {		
		return productoRepositoryCustom.insertProducto(producto);
	}
	
}
