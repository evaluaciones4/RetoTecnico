package com.retotecnico.service;

import com.retotecnico.model.Producto;
import com.retotecnico.model.ResultInsertProducto;

public interface ProductoService {

	/**
	 * Inserta un Nuevo Producto
	 * @param producto: Instacia del Producto
	 * @return Lista de todos los productos
	 * @throws Exception
	 */
	ResultInsertProducto insertProducto (Producto producto)throws Exception;
}
