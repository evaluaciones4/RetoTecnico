package com.retotecnico.repository.custom.impl;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.retotecnico.model.Authority;
import com.retotecnico.model.Usuario;
import com.retotecnico.repository.custom.UsuarioRepositoryCustom;
import com.retotecnico.util.AuthorityName;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@org.springframework.stereotype.Component("usuarioRepository")
public class UsuarioRepositoryCustomImpl implements UsuarioRepositoryCustom{

	@Override
	public Optional<Usuario> buscarByUserName(String userName) throws Exception {
		log.info("UserName: "+ userName);
	
		Usuario usuario = null;
		
		//Para efectos de demo, se maneja de manera estática 
		// if(userName.equals("retotecnico")) {
			String cryptoBC = new BCryptPasswordEncoder().encode("123**##");
			Authority authority = new Authority();
			authority.setName(AuthorityName.ADMIN);
			
			usuario = new Usuario();
			usuario.setLogin(userName);
			usuario.setPassword(cryptoBC);
			usuario.setAuthorities(Arrays.asList(authority));
			
		// }
		
		return Optional.ofNullable(usuario);
	}
	
}
