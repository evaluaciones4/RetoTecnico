package com.retotecnico.repository.custom.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.retotecnico.DataBaseConnection;
import com.retotecnico.model.Producto;
import com.retotecnico.model.ResultInsertProducto;
import com.retotecnico.repository.custom.ProductoRepositoryCustom;


@org.springframework.stereotype.Component("productoRepositoryCustom")
public class ProductoRepositoryCustomImpl extends DataBaseConnection implements ProductoRepositoryCustom{
	
	@SuppressWarnings("unchecked")
	@Override
	public ResultInsertProducto insertProducto(Producto producto) throws Exception {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getJdbc())
				.withProcedureName("sp_insertAndListProducts")
				.declareParameters(
						new SqlParameter("v_idProducto", Types.NUMERIC),
						new SqlParameter("v_nombre", Types.NVARCHAR),
						new SqlParameter("v_fechaRegistro", Types.NVARCHAR),
						new SqlOutParameter("codigoRespuesta", Types.INTEGER),
                        new SqlOutParameter("mensajeRespuesta", Types.VARCHAR)
						);
		
		Map<String, Object> inParams = new HashMap<>();
        inParams.put("v_idProducto", producto.getIdProducto());
        inParams.put("v_nombre", producto.getNombre());
        inParams.put("v_fechaRegistro", producto.getFechaRegistro());
		
        Map<String, Object> outParams = jdbcCall.execute(inParams);
        
        Integer codigoRespuesta = (Integer) outParams.get("codigoRespuesta");
        String mensajeRespuesta = outParams.get("mensajeRespuesta").toString();
        List<Producto> listProductos = new ArrayList<Producto>();
        
        if(codigoRespuesta == 0) {
        	Map<String, Object> map = new HashMap<String, Object>();
            List<?> cur_listProductos = (List<?>) outParams.get("CUR_LISTPRODUCTOS");            
            for(int i=0; i < cur_listProductos.size(); i++){
    			map = (Map<String, Object>)cur_listProductos.get(i);
    			Producto _producto = new Producto();
    			_producto.setIdProducto(((BigDecimal)map.get("idProducto")).longValue());
    			_producto.setNombre(map.get("nombre").toString());
    			_producto.setFechaRegistro(map.get("fechaRegistro").toString());
    			
    			listProductos.add(_producto);
            }
        }
        
        //
        return  new ResultInsertProducto(codigoRespuesta, mensajeRespuesta, listProductos);        
	}

}
