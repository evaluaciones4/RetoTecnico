package com.retotecnico.repository.custom;

import java.util.Optional;

import com.retotecnico.model.Usuario;

public interface UsuarioRepositoryCustom {
	/**
	 * 
	 * @param login
	 * @return
	 * @throws Exception
	 */
	Optional<Usuario> buscarByUserName(String userName)throws Exception;
}
