package com.retotecnico;

import java.io.IOException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DataBaseConnection{	

	/**
	 * @return the jdbcTemplate
	 * @throws IOException 
	 */
	public JdbcTemplate getJdbc() throws IOException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();			
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");			
		dataSource.setUrl("jdbc:oracle:thin:@9.9.9.9:1521:CDB1");	//Url
		dataSource.setUsername("username"); //Nombre de Usuario
		dataSource.setPassword("password"); //Password
		JdbcTemplate jdbcTemplate =new JdbcTemplate(dataSource);
				
		return jdbcTemplate;
	}
}
