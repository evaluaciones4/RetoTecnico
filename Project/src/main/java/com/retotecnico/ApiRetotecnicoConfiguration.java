package com.retotecnico;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class ApiRetotecnicoConfiguration {
		
	@Bean(name = "passwordEncoder")
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean(name = "securityFilterChain")
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests(authorize -> authorize
                        .antMatchers("/swagger-ui/**").permitAll()
                        .anyRequest().authenticated()
        )
        .formLogin(form -> form.defaultSuccessUrl("/swagger-ui/index.html", true))
        // .logout(logout ->
		//         logout.logoutUrl("/logout") 
		//                .logoutSuccessUrl("/login") 
		//                .invalidateHttpSession(true)
		//                .deleteCookies("JSESSIONID")
        // 	   )
        .csrf(AbstractHttpConfigurer::disable)
        .httpBasic(Customizer.withDefaults())
        //.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) //Uso sin session
        
        ;             		  

        return http.build();
    }
	
}
