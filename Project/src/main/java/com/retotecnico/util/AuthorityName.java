package com.retotecnico.util;

public enum AuthorityName {
	READ, WRITE, ADMIN
}
