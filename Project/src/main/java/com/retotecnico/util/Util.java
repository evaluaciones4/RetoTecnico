package com.retotecnico.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Util {

	public static boolean isNumeric(String cadena){
		try {
			Long.parseLong(cadena);
			
			return true;
		} catch (NumberFormatException nfe){			
			try {
				Integer.parseInt(cadena);
				
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}
	

	public static boolean isValidFormatoFecha(String value, String formato) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
			LocalDate.parse(value, formatter);
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}

		
}
