package com.retotecnico;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.PropertySource;

import com.retotecnico.model.Producto;
import com.retotecnico.model.ResultInsertProducto;
import com.retotecnico.repository.custom.impl.ProductoRepositoryCustomImpl;

@PropertySource("classpath:application.properties")
class RetotecnicoApplicationTests {

	@InjectMocks
    private ProductoRepositoryCustomImpl productoCustomImpl;

    LocalDate currentDate = LocalDate.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    String fechaRegistro = currentDate.format(formatter);

	@BeforeEach
	public void setUp(){
		MockitoAnnotations.openMocks(this);
	}

	@Test
    public void testInsertarProductoExito() throws Exception {
        Random rand = new Random();
        Long _idProducto = (long) rand.nextInt(10000);
        
        Producto producto = new Producto();
        producto.setIdProducto(_idProducto);
        producto.setNombre("Producto "+ producto.getIdProducto().toString());
        producto.setFechaRegistro(fechaRegistro);
        
        ResultInsertProducto result = productoCustomImpl.insertProducto(producto);

        assertNotNull(result); 
        assertEquals(0, result.getCodigoRespuesta());
        assertEquals("Ejecución con éxito", result.getMensajeRespuesta());
    }

    @Test
    public void testInsertarProductoError() throws Exception {
        Producto producto = new Producto();
        producto.setIdProducto(null);
        producto.setNombre("Producto 1");
        producto.setFechaRegistro(fechaRegistro);

        ResultInsertProducto result = productoCustomImpl.insertProducto(producto);

        assertNotNull(result); 
        assertEquals(1, result.getCodigoRespuesta());
    }
}
