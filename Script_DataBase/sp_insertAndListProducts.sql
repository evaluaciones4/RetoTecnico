CREATE OR REPLACE PROCEDURE sp_insertAndListProducts
       (
        v_idProducto IN numeric,
        v_nombre  IN nvarchar2,
        v_fechaRegistro  IN nvarchar2,
        cur_listProductos OUT SYS_REFCURSOR,
        codigoRespuesta OUT numeric,
        mensajeRespuesta OUT nvarchar2
        )
      AS
        v_count NUMBER;
        
      BEGIN
         SAVEPOINT sp_start;
         
         BEGIN
         OPEN cur_listProductos for select * from dual;       
         
        --> Verificar si el idProducto ya existe
        SELECT COUNT(*) INTO v_count FROM producto WHERE idProducto = v_idProducto;                
        
        if(v_count > 0) then
           codigoRespuesta := 1;
           mensajeRespuesta := 'El valor del identificador del producto '|| v_idProducto || ', ya est� registrado.';
        else
          --> Insertando los productos
          insert into producto
            (idproducto, nombre, fecharegistro)
          values
            (v_idproducto, v_nombre, v_fecharegistro);    

          -->Cursor para los productos
          OPEN cur_listProductos FOR
          SELECT * FROM PRODUCTO WHERE fechaRegistro = v_fechaRegistro ORDER BY idProducto ASC;
          
          -- Establecer c�digo y mensaje de respuesta en caso de �xito
          codigoRespuesta := 0;
          mensajeRespuesta := 'Ejecuci�n con �xito';

          -- Commit de la transacci�n
          COMMIT;
        end if;        
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK TO sp_start;
            codigoRespuesta := 1;
            mensajeRespuesta := 'Error ejecutando el procedimiento: ' || SQLERRM;
        END;

      END sp_insertAndListProducts;
/
