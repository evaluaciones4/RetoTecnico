-- Create table
create table PRODUCTO
(
  idproducto    NUMBER(5) not null,
  nombre        NVARCHAR2(150),
  fecharegistro NVARCHAR2(10)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column PRODUCTO.idproducto
  is 'Identificador unico de la entidad, tamanio de la clave 5';
comment on column PRODUCTO.nombre
  is 'Nombre del producto';
comment on column PRODUCTO.fecharegistro
  is 'Fecha de registro del producto';
-- Create/Recreate primary, unique and foreign key constraints 
alter table PRODUCTO
  add constraint PK_PRODUCTO primary key (IDPRODUCTO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
